package com.kytms.transportorder.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 陈小龙
 * 分段订单货品明细
 *  2018-03-23
 */
public interface LedProductDao<LedProduct> extends BaseDao<LedProduct> {
}
